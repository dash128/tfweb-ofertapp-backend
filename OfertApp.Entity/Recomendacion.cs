namespace OfertApp.Entity
{
    public class Recomendacion
    {
        public int Id {get; set;}
        public string Comentario {get; set;}
        
        public int PostulanteId {get; set;}
        public Postulante Postulante {get; set;}
        public int EmpresaId {get; set;}
        public Empresa Empresa {get; set;}
    }
}