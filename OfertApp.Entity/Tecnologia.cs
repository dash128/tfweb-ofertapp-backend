namespace OfertApp.Entity
{
    public class Tecnologia
    {
        public int Id {get; set;}
        public string Nombre {get; set;}
        public string Icono {get; set;}
    }
}