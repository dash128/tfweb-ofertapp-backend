namespace OfertApp.Entity
{
    public class Inscripcion
    {
        public int Id {get; set;}

        public int PostulanteId {get; set;}
        public Postulante Postulante {get; set;}
        public int TrabajoId {get; set;}
        public Trabajo Trabajo {get; set;}
        public int EstadoId {get; set;}
        public Estado Estado {get; set;}
    }
}