namespace OfertApp.Entity
{
    public class Requisito
    {
        public int Id{get; set;}

        
        public int TrabajoId {get; set;}
        public Trabajo Trabajo {get; set;}
        public int TecnologiaId {get; set;}
        public Tecnologia Tecnologia {get; set;}
        public int NivelId {get; set;}
        public Nivel Nivel {get; set;}
    }
}