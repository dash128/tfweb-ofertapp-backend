using System;

namespace OfertApp.Entity
{
    public class Empresa
    {
        public int Id {get; set;}
        public string Nombre {get; set;}
        public string Celular {get; set;}
        public string Direccion {get; set;}
        public string Dni {get; set;}
        public DateTime FechaNacimiento {get; set;}

        public string Correo {get; set;}
        public string Contrasena {get; set;}
    }
}