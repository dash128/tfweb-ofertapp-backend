using System;

namespace OfertApp.Entity
{
    public class Trabajo
    {
        public int Id {get; set;}
        public string Titulo {get; set;}
        public string Descripcion {get; set;}
        public DateTime FechaPublicacion {get; set;}
        public decimal Remuneracion {get; set;}
        public bool Publicado {get; set;}
        
        public int TipoTrabajoId {get; set;}
        public TipoTrabajo TipoTrabajo {get; set;}
        public int EmpresaId {get; set;}
        public Empresa Empresa {get; set;}
    }
}