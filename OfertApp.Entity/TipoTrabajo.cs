namespace OfertApp.Entity
{
    public class TipoTrabajo
    {
        public int Id {get; set;}
        public string Nombre {get; set;}
        public string Descripcion {get; set;}
        public decimal Comision {get; set;}
    }
}