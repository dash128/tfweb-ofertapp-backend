namespace OfertApp.Entity
{
    public class Conocimiento
    {
        public int Id{get; set;}

        
        public int PostulanteId {get; set;}
        public Postulante Postulante {get; set;}
        public int TecnologiaId {get; set;}
        public Tecnologia Tecnologia {get; set;}
        public int NivelId {get; set;}
        public Nivel Nivel {get; set;}

    }
}