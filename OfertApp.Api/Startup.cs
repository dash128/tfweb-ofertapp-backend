﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OfertApp.Repository;
using OfertApp.Repository.context;
using OfertApp.Repository.implementation;
using OfertApp.Service;
using OfertApp.Service.implementation;
using Swashbuckle.AspNetCore.Swagger;

namespace OfertApp.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options=>options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            // services.AddDbContext
            services.AddTransient<IConocimientoRepository, ConocimientoRepository> ();
            services.AddTransient<IConocimientoService, ConocimientoService> ();
            services.AddTransient<IEmpresaRepository, EmpresaRepository>();
            services.AddTransient<IEmpresaService, EmpresaService>();
            services.AddTransient<IEstadoRepository, EstadoRepository>();
            services.AddTransient<IEstadoService, EstadoService>();
            services.AddTransient<IInscripcionRepository, InscripcionRepository>();
            services.AddTransient<IInscripcionService, InscripcionService>();
            services.AddTransient<INivelRepository, NivelRepository>();
            services.AddTransient<INivelService, NivelService>();
            services.AddTransient<IPostulanteRepository, PostulanteRepository>();
            services.AddTransient<IPostulanteService, PostulanteService>();
            services.AddTransient<IRecomendacionRepository, RecomendacionRepository>();
            services.AddTransient<IRecomendacionService, RecomendacionService>();
            services.AddTransient<IRequisitoRepository, RequisitoRepository>();
            services.AddTransient<IRequisitoService, RequisitoService>();
            services.AddTransient<ITecnologiaRepository, TecnologiaRepository>();
            services.AddTransient<ITecnologiaService, TecnologiaService>();
            services.AddTransient<ITipoTrabajoRepository, TipoTrabajoRepository>();
            services.AddTransient<ITipoTrabajoService, TipoTrabajoService>();
            services.AddTransient<ITrabajoRepository, TrabajoRepository>();
            services.AddTransient<ITrabajoService, TrabajoService>();
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // https://localhost:5001/swagger/index.html
            services.AddSwaggerGen (swagger => {
                var contact = new Contact () { Name = SwaggerConfiguration.ContactName, Url = SwaggerConfiguration.ContactUrl };
                swagger.SwaggerDoc (SwaggerConfiguration.DocNameV1,
                    new Info {
                        Title = SwaggerConfiguration.DocInfoTitle,
                            Version = SwaggerConfiguration.DocInfoVersion,
                            Description = SwaggerConfiguration.DocInfoDescription,
                            Contact = contact
                    }
                );
            });

            services.AddCors (options => {
                options.AddPolicy ("Todos",
                    builder => builder.WithOrigins ("*").WithHeaders ("*").WithMethods ("*"));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint (SwaggerConfiguration.EndpointUrl, SwaggerConfiguration.EndpointDescription);
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors ("Todos");
            // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
