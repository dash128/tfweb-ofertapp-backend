namespace OfertApp.Api
{
    public class SwaggerConfiguration
    {
        public const string EndpointDescription = "OfetApp API v1";
        public const string EndpointUrl = "/swagger/v1/swagger.json";
        public const string ContactName = "SAsuke Uchiha";
        public const string ContactUrl = "http://TeamGaaa.com";
        public const string DocNameV1 = "v1";
        public const string DocInfoTitle = "OfetApp API";
        public const string DocInfoVersion = "v1";
        public const string DocInfoDescription = "OfetApp Api";
    }
}