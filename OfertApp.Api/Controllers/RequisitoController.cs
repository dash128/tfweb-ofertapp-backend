using Microsoft.AspNetCore.Mvc;
using OfertApp.Entity;
using OfertApp.Service;

namespace OfertApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequisitoController: ControllerBase
    {
        private IRequisitoService requisitoService;
        public RequisitoController(IRequisitoService requisitoService){
            this.requisitoService = requisitoService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(requisitoService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Requisito requisito){
            return Ok(requisitoService.Guardar(requisito));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Requisito requisito){
            return Ok(requisitoService.Actualizar(requisito));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(requisitoService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(requisitoService.ListarPorId(id));
        }
    }
}