using Microsoft.AspNetCore.Mvc;
using OfertApp.Entity;
using OfertApp.Service;

namespace OfertApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoController: ControllerBase
    {
        private IEstadoService estadoService;
        public EstadoController(IEstadoService estadoService){
            this.estadoService = estadoService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(estadoService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Estado estado){
            return Ok(estadoService.Guardar(estado));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Estado estado){
            return Ok(estadoService.Actualizar(estado));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(estadoService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(estadoService.ListarPorId(id));
        }
    }
}