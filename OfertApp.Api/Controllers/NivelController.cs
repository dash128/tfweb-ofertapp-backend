using Microsoft.AspNetCore.Mvc;
using OfertApp.Entity;
using OfertApp.Service;

namespace OfertApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NivelController: ControllerBase
    {
        private INivelService nivelService;
        public NivelController(INivelService nivelService){
            this.nivelService = nivelService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(nivelService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Nivel nivel){
            return Ok(nivelService.Guardar(nivel));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Nivel nivel){
            return Ok(nivelService.Actualizar(nivel));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(nivelService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(nivelService.ListarPorId(id));
        }
    }
}