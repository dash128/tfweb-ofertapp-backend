using Microsoft.AspNetCore.Mvc;
using OfertApp.Entity;
using OfertApp.Service;

namespace OfertApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoTrabajoController: ControllerBase
    {
        private ITipoTrabajoService tipoTrabajoService;
        public TipoTrabajoController(ITipoTrabajoService tipoTrabajoService){
            this.tipoTrabajoService = tipoTrabajoService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(tipoTrabajoService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] TipoTrabajo tipoTrabajo){
            return Ok(tipoTrabajoService.Guardar(tipoTrabajo));
        }
        [HttpPut]
        public ActionResult Put([FromBody] TipoTrabajo tipoTrabajo){
            return Ok(tipoTrabajoService.Actualizar(tipoTrabajo));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(tipoTrabajoService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(tipoTrabajoService.ListarPorId(id));
        }
    }
}