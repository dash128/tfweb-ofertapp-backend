using Microsoft.AspNetCore.Mvc;
using OfertApp.Entity;
using OfertApp.Service;

namespace OfertApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TecnologiaController: ControllerBase
    {
        private ITecnologiaService tecnologiaService;
        public TecnologiaController(ITecnologiaService tecnologiaService){
            this.tecnologiaService = tecnologiaService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(tecnologiaService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Tecnologia tecnologia){
            return Ok(tecnologiaService.Guardar(tecnologia));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Tecnologia tecnologia){
            return Ok(tecnologiaService.Actualizar(tecnologia));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(tecnologiaService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(tecnologiaService.ListarPorId(id));
        }
    }
}