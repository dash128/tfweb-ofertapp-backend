using Microsoft.AspNetCore.Mvc;
using OfertApp.Entity;
using OfertApp.Service;

namespace OfertApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostulanteController: ControllerBase
    {
        private IPostulanteService postulanteService;
        public PostulanteController(IPostulanteService postulanteService){
            this.postulanteService = postulanteService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(postulanteService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Postulante postulante){
            return Ok(postulanteService.Guardar(postulante));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Postulante postulante){
            return Ok(postulanteService.Actualizar(postulante));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(postulanteService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(postulanteService.ListarPorId(id));
        }

        [HttpPost("validate")]
        public ActionResult ValidarCuenta([FromBody] Postulante postulante){
            return Ok(postulanteService.ValidarCorreo(postulante.Correo, postulante.Contrasena));
        }
    }
}