using Microsoft.AspNetCore.Mvc;
using OfertApp.Entity;
using OfertApp.Service;

namespace OfertApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrabajoController: ControllerBase
    {
        private ITrabajoService trabajoService;
        public TrabajoController(ITrabajoService trabajoService){
            this.trabajoService = trabajoService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(trabajoService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Trabajo trabajo){
            return Ok(trabajoService.Guardar(trabajo));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Trabajo trabajo){
            return Ok(trabajoService.Actualizar(trabajo));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(trabajoService.Eliminar(id));
        }
        [HttpGet("{id}")]
        public ActionResult Get([FromRoute] int id){
            return Ok(trabajoService.ListarPorId(id));
        }


        [HttpGet("PorEmpresa/{id}")]
        public ActionResult GetTrabajoPorEmpresa([FromRoute] int id){
            return Ok(trabajoService.TrabajoPorEmpresa(id));
        }

        [HttpGet("Publicado")]
        public ActionResult GetTrabajoPublicado(){
            return Ok(trabajoService.TrabajoPublicado());
        }

        [HttpGet("Publicado")]
        public ActionResult GetTrabajoPublicado2(){
            return Ok(trabajoService.TrabajoPublicado2(2));
        }


        [HttpGet("PorPostulante/{id}")]
        public ActionResult GetTrabajoPorPostulante([FromRoute] int id){
            return Ok(trabajoService.TrabajoPorPostulante(id));
        }
    }
}