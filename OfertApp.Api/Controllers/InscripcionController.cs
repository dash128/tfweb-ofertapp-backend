using Microsoft.AspNetCore.Mvc;
using OfertApp.Entity;
using OfertApp.Service;

namespace OfertApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InscripcionController: ControllerBase
    {
        private IInscripcionService inscripcionService;
        public InscripcionController(IInscripcionService inscripcionService){
            this.inscripcionService = inscripcionService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(inscripcionService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Inscripcion inscripcion){
            return Ok(inscripcionService.Guardar(inscripcion));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Inscripcion inscripcion){
            return Ok(inscripcionService.Actualizar(inscripcion));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(inscripcionService.Eliminar(id));
        }
        [HttpGet("{id}")]
        public ActionResult Get([FromRoute] int id){
            return Ok(inscripcionService.ListarPorId(id));
        }


        [HttpGet("PorTrabajo/{id}")]
        public ActionResult GetInscripcionPorTrabajo([FromRoute] int id){
            return Ok(inscripcionService.InscripcionPorTrabajo(id));
        }

        [HttpGet("PorPostulante/{id}")]
        public ActionResult GetInscripcionPorPostulante([FromRoute] int id){
            return Ok(inscripcionService.InscripcionPorPostulante(id));
        }

        [HttpGet("PorEmpresa/{id}")]
        public ActionResult GetInscripcionPorEmpresa([FromRoute] int id){
            return Ok(inscripcionService.InscripcionPorEmpresa(id));
        }

        [HttpGet("PorPostulante/{estadoId}/{postulanteId}")]
        public ActionResult GetInscripcionPorPostulanteAndEstado([FromRoute] int estadoId, [FromRoute] int postulanteId){
            return Ok(inscripcionService.InscripcionPorEstadoAndPostulante(estadoId, postulanteId));
        }
        

    }
}