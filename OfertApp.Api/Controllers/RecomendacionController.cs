using Microsoft.AspNetCore.Mvc;
using OfertApp.Entity;
using OfertApp.Service;

namespace OfertApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecomendacionController: ControllerBase
    {
        private IRecomendacionService recomendacionService;
        public RecomendacionController(IRecomendacionService recomendacionService){
            this.recomendacionService = recomendacionService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(recomendacionService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Recomendacion recomendacion){
            return Ok(recomendacionService.Guardar(recomendacion));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Recomendacion recomendacion){
            return Ok(recomendacionService.Actualizar(recomendacion));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(recomendacionService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(recomendacionService.ListarPorId(id));
        }
    }
}