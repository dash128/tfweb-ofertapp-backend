using Microsoft.AspNetCore.Mvc;
using OfertApp.Entity;
using OfertApp.Service;

namespace OfertApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresaController: ControllerBase
    {
        private IEmpresaService empresaService;
        public EmpresaController(IEmpresaService empresaService){
            this.empresaService = empresaService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(empresaService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Empresa empresa){
            return Ok(empresaService.Guardar(empresa));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Empresa empresa){
            return Ok(empresaService.Actualizar(empresa));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(empresaService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(empresaService.ListarPorId(id));
        }

        
        [HttpPost("validate")]
        public ActionResult ValidarCuenta([FromBody] Empresa empresa){
            return Ok(empresaService.ValidarCorreo(empresa.Correo, empresa.Contrasena));
        }
    }
}