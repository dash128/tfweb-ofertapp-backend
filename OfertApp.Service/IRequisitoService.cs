using System.Collections.Generic;
using OfertApp.Entity;

namespace OfertApp.Service
{
    public interface IRequisitoService: ICrudService<Requisito>
    {
         IEnumerable<Requisito> RequisitoPorPostulante(int id);
    }
}