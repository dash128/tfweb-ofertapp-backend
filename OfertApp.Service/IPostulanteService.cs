using System.Collections;
using OfertApp.Entity;

namespace OfertApp.Service
{
    public interface IPostulanteService: ICrudService<Postulante>
    {
        object ValidarCorreo(string correo, string contrasena);
    }
}