using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class RequisitoService : IRequisitoService
    {
        private IRequisitoRepository requisitoRepository;
        public RequisitoService(IRequisitoRepository requisitoRepository){
            this.requisitoRepository=requisitoRepository;
        }


        public bool Actualizar(Requisito entity)
        {
            return requisitoRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return requisitoRepository.Eliminar(id);
        }

        public bool Guardar(Requisito entity)
        {
            return requisitoRepository.Guardar(entity);
        }

        public IEnumerable<Requisito> Listar()
        {
            return requisitoRepository.Listar();
        }

        public Requisito ListarPorId(int id)
        {
            return requisitoRepository.ListarPorId(id);
        }

        public IEnumerable<Requisito> RequisitoPorPostulante(int id)
        {
            return requisitoRepository.RequisitoPorPostulante(id);
        }
    }
}