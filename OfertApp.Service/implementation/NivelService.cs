using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class NivelService : INivelService
    {
        private INivelRepository nivelRepository;
        public NivelService(INivelRepository nivelRepository){
            this.nivelRepository=nivelRepository;
        }


        public bool Actualizar(Nivel entity)
        {
            return nivelRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return nivelRepository.Eliminar(id);
        }

        public bool Guardar(Nivel entity)
        {
            return nivelRepository.Guardar(entity);
        }

        public IEnumerable<Nivel> Listar()
        {
            return nivelRepository.Listar();
        }

        public Nivel ListarPorId(int id)
        {
            return nivelRepository.ListarPorId(id);
        }
    }
}