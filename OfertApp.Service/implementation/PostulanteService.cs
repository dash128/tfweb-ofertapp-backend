using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class PostulanteService : IPostulanteService
    {
        private IPostulanteRepository postulanteRepository;
        public PostulanteService(IPostulanteRepository postulanteRepository){
            this.postulanteRepository=postulanteRepository;
        }


        public bool Actualizar(Postulante entity)
        {
            return postulanteRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return postulanteRepository.Eliminar(id);
        }

        public bool Guardar(Postulante entity)
        {
            return postulanteRepository.Guardar(entity);
        }

        public IEnumerable<Postulante> Listar()
        {
            return postulanteRepository.Listar();
        }

        public Postulante ListarPorId(int id)
        {
            return postulanteRepository.ListarPorId(id);
        }

        public object ValidarCorreo(string correo, string contrasena)
        {
            return postulanteRepository.ValidarCorreo(correo, contrasena);
        }
    }
}