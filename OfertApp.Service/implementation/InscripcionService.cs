using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class InscripcionService : IInscripcionService
    {
        private IInscripcionRepository inscripcionRepository;
        public InscripcionService(IInscripcionRepository inscripcionRepository){
            this.inscripcionRepository=inscripcionRepository;
        }


        public bool Actualizar(Inscripcion entity)
        {
            return inscripcionRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return inscripcionRepository.Eliminar(id);
        }

        public bool Guardar(Inscripcion entity)
        {
            return inscripcionRepository.Guardar(entity);
        }

        public IEnumerable<Inscripcion> InscripcionPorEmpresa(int id)
        {
            return inscripcionRepository.InscripcionPorEmpresa(id);
        }

        public IEnumerable<Inscripcion> InscripcionPorEstadoAndPostulante(int estadoId, int postulanteId)
        {
            return inscripcionRepository.InscripcionPorEstadoAndPostulante(estadoId, postulanteId);
        }

        public IEnumerable<Inscripcion> InscripcionPorPostulante(int id)
        {
            return inscripcionRepository.InscripcionPorPostulante(id);
        }

        public IEnumerable<Inscripcion> InscripcionPorTrabajo(int id)
        {
            return inscripcionRepository.InscripcionPorTrabajo(id);
        }

        public IEnumerable<Inscripcion> Listar()
        {
            return inscripcionRepository.Listar();
        }

        public Inscripcion ListarPorId(int id)
        {
            return inscripcionRepository.ListarPorId(id);
        }
    }
}