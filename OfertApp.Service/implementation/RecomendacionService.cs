using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class RecomendacionService : IRecomendacionService
    {
        private IRecomendacionRepository recomendacionRepository;
        public RecomendacionService(IRecomendacionRepository recomendacionRepository){
            this.recomendacionRepository=recomendacionRepository;
        }


        public bool Actualizar(Recomendacion entity)
        {
            return recomendacionRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return recomendacionRepository.Eliminar(id);
        }

        public bool Guardar(Recomendacion entity)
        {
            return recomendacionRepository.Guardar(entity);
        }

        public IEnumerable<Recomendacion> Listar()
        {
            return recomendacionRepository.Listar();
        }

        public Recomendacion ListarPorId(int id)
        {
            return recomendacionRepository.ListarPorId(id);
        }
    }
}