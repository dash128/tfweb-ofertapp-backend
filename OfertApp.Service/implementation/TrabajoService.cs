using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class TrabajoService : ITrabajoService
    {
        private ITrabajoRepository trabajoRepository;
        public TrabajoService(ITrabajoRepository trabajoRepository){
            this.trabajoRepository=trabajoRepository;
        }


        public bool Actualizar(Trabajo entity)
        {
            return trabajoRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return trabajoRepository.Eliminar(id);
        }

        public bool Guardar(Trabajo entity)
        {
            return trabajoRepository.Guardar(entity);
        }

        public IEnumerable<Trabajo> Listar()
        {
            return trabajoRepository.Listar();
        }

        public Trabajo ListarPorId(int id)
        {
            return trabajoRepository.ListarPorId(id);
        }

        public IEnumerable<Trabajo> TrabajoPorEmpresa(int id)
        {
            return trabajoRepository.TrabajoPorEmpresa(id);
        }

        public IEnumerable<Trabajo> TrabajoPorPostulante(int id)
        {
            return trabajoRepository.TrabajoPorPostulante(id);
        }

        public IEnumerable<Trabajo> TrabajoPublicado()
        {
            return trabajoRepository.TrabajoPublicado();
        }

        public IEnumerable<Trabajo> TrabajoPublicado2(int postulanteId)
        {
            return trabajoRepository.TrabajoPublicado2(postulanteId);
        }
    }
}