using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class ConocimientoService : IConocimientoService
    {
        private IConocimientoRepository conocimientoRepository;
        public ConocimientoService(IConocimientoRepository conocimientoRepository){
            this.conocimientoRepository=conocimientoRepository;
        }


        public bool Actualizar(Conocimiento entity)
        {
            return conocimientoRepository.Actualizar(entity);
        }

        public IEnumerable<Conocimiento> ConocimientoPorPostulante(int id)
        {
           return conocimientoRepository.ConocimientoPorPostulante(id);
        }

        public bool Eliminar(int id)
        {
            return conocimientoRepository.Eliminar(id);
        }

        public bool Guardar(Conocimiento entity)
        {
            return conocimientoRepository.Guardar(entity);
        }

        public IEnumerable<Conocimiento> Listar()
        {
            return conocimientoRepository.Listar();
        }

        public Conocimiento ListarPorId(int id)
        {
            return conocimientoRepository.ListarPorId(id);
        }
    }
}