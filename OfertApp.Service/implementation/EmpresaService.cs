using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class EmpresaService : IEmpresaService
    {
        private IEmpresaRepository empresaRepository;
        public EmpresaService(IEmpresaRepository empresaRepository){
            this.empresaRepository=empresaRepository;
        }


        public bool Actualizar(Empresa entity)
        {
            return empresaRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return empresaRepository.Eliminar(id);
        }

        public bool Guardar(Empresa entity)
        {
            return empresaRepository.Guardar(entity);
        }

        public IEnumerable<Empresa> Listar()
        {
            return empresaRepository.Listar();
        }

        public Empresa ListarPorId(int id)
        {
            return empresaRepository.ListarPorId(id);
        }

        public object ValidarCorreo(string correo, string contrasena)
        {
            return empresaRepository.ValidarCorreo(correo, contrasena);
        }
    }
}