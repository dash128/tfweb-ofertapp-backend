using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class EstadoService : IEstadoService
    {
        private IEstadoRepository estadoRepository;
        public EstadoService(IEstadoRepository estadoRepository){
            this.estadoRepository=estadoRepository;
        }


        public bool Actualizar(Estado entity)
        {
            return estadoRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return estadoRepository.Eliminar(id);
        }

        public bool Guardar(Estado entity)
        {
            return estadoRepository.Guardar(entity);
        }

        public IEnumerable<Estado> Listar()
        {
            return estadoRepository.Listar();
        }

        public Estado ListarPorId(int id)
        {
            return estadoRepository.ListarPorId(id);
        }
    }
}