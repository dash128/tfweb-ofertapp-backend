using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class TecnologiaService : ITecnologiaService
    {
        private ITecnologiaRepository tecnologiaRepository;
        public TecnologiaService(ITecnologiaRepository tecnologiaRepository){
            this.tecnologiaRepository=tecnologiaRepository;
        }


        public bool Actualizar(Tecnologia entity)
        {
            return tecnologiaRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return tecnologiaRepository.Eliminar(id);
        }

        public bool Guardar(Tecnologia entity)
        {
            return tecnologiaRepository.Guardar(entity);
        }

        public IEnumerable<Tecnologia> Listar()
        {
            return tecnologiaRepository.Listar();
        }

        public Tecnologia ListarPorId(int id)
        {
            return tecnologiaRepository.ListarPorId(id);
        }
    }
}