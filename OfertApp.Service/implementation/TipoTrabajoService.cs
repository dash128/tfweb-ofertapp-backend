using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository;

namespace OfertApp.Service.implementation
{
    public class TipoTrabajoService : ITipoTrabajoService
    {
        private ITipoTrabajoRepository tipoTrabajoRepository;
        public TipoTrabajoService(ITipoTrabajoRepository tipoTrabajoRepository){
            this.tipoTrabajoRepository=tipoTrabajoRepository;
        }


        public bool Actualizar(TipoTrabajo entity)
        {
            return tipoTrabajoRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return tipoTrabajoRepository.Eliminar(id);
        }

        public bool Guardar(TipoTrabajo entity)
        {
            return tipoTrabajoRepository.Guardar(entity);
        }

        public IEnumerable<TipoTrabajo> Listar()
        {
            return tipoTrabajoRepository.Listar();
        }

        public TipoTrabajo ListarPorId(int id)
        {
            return tipoTrabajoRepository.ListarPorId(id);
        }
    }
}