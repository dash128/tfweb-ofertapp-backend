using System.Collections.Generic;
using OfertApp.Entity;

namespace OfertApp.Service
{
    public interface IInscripcionService: ICrudService<Inscripcion>
    {
         IEnumerable<Inscripcion> InscripcionPorTrabajo(int id);
         IEnumerable<Inscripcion> InscripcionPorPostulante(int id);
         IEnumerable<Inscripcion> InscripcionPorEmpresa(int id);
         IEnumerable<Inscripcion> InscripcionPorEstadoAndPostulante(int estadoId, int postulanteId);

    }
}