using System.Collections.Generic;
using OfertApp.Entity;

namespace OfertApp.Service
{
    public interface IConocimientoService: ICrudService<Conocimiento>
    {
         IEnumerable<Conocimiento> ConocimientoPorPostulante(int id);
    }
}