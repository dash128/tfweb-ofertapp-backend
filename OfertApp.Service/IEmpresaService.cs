using OfertApp.Entity;

namespace OfertApp.Service
{
    public interface IEmpresaService: ICrudService<Empresa>
    {
         object ValidarCorreo(string correo, string contrasena);
    }
}