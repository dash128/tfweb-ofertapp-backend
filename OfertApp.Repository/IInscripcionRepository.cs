using System.Collections.Generic;
using OfertApp.Entity;

namespace OfertApp.Repository
{
    public interface IInscripcionRepository: ICrudRepository<Inscripcion>
    {
        IEnumerable<Inscripcion> InscripcionPorTrabajo(int id);
         IEnumerable<Inscripcion> InscripcionPorPostulante(int id);
         IEnumerable<Inscripcion> InscripcionPorEmpresa(int id);
         IEnumerable<Inscripcion> InscripcionPorEstadoAndPostulante(int estadoId, int postulanteId);
    }
}