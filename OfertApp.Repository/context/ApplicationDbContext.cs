using OfertApp.Entity;
using Microsoft.EntityFrameworkCore;

namespace OfertApp.Repository.context
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<Conocimiento> Conocimiento { get; set; }
        public DbSet<Empresa> Empresa { get; set; }
        public DbSet<Estado> Estado { get; set; }
        public DbSet<Inscripcion> Inscripcion { get; set; }
        public DbSet<Nivel> Nivel { get; set; }
        public DbSet<Postulante> Postulante { get; set; }
        public DbSet<Recomendacion> Recomendacion { get; set; }
        public DbSet<Requisito> Requisito { get; set; }
        public DbSet<Tecnologia> Tecnologia { get; set; }
        public DbSet<TipoTrabajo> TipoTrabajo { get; set; }
        public DbSet<Trabajo> Trabajo { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options){

        }

        // protected override void OnModelCreating (ModelBuilder modelBuilder){
        //     modelBuilder.Entity<Empresa> ()
        //         .Property (n=>n.Nombre)
        //         .HasConversion ("Nombre")
        //         .HasMaxLength (50)
        //         .IsRe
        // }
    }
}