using System.Collections.Generic;
using OfertApp.Entity;

namespace OfertApp.Repository
{
    public interface IRequisitoRepository: ICrudRepository<Requisito>
    {
         IEnumerable<Requisito> RequisitoPorPostulante(int id);
    }
}