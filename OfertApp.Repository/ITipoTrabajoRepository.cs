
using OfertApp.Entity;

namespace OfertApp.Repository
{
    public interface ITipoTrabajoRepository: ICrudRepository<TipoTrabajo>
    {
         
    }
}