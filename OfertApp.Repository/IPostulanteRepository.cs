using OfertApp.Entity;

namespace OfertApp.Repository
{
    public interface IPostulanteRepository: ICrudRepository<Postulante>
    {
        object ValidarCorreo(string correo, string contrasena);
    }
}