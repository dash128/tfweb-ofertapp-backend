using OfertApp.Entity;

namespace OfertApp.Repository
{
    public interface IEmpresaRepository: ICrudRepository<Empresa>
    {
        object ValidarCorreo(string correo, string contrasena);
    }
}