

-- Tabla Tecnologia
INSERT INTO Tecnologia (Nombre, Icono) VALUES (
'C++','https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/306px-ISO_C%2B%2B_Logo.svg.png');
INSERT INTO Tecnologia (Nombre, Icono) VALUES (
'C#','https://es.wikipedia.org/wiki/C_Sharp#/media/Archivo:Csharp_Logo.png');
INSERT INTO Tecnologia (Nombre, Icono) VALUES (
'Net.Core','https://www.muylinux.com/wp-content/uploads/2017/08/NET.png');
INSERT INTO Tecnologia (Nombre, Icono) VALUES (
'Java','https://upload.wikimedia.org/wikipedia/commons/d/de/%D0%9B%D0%BE%D0%B3%D0%BE_%D0%B6%D0%B0%D0%B2%D0%B0.png');
INSERT INTO Tecnologia (Nombre, Icono) VALUES (
'Php','https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1024px-PHP-logo.svg.png');
INSERT INTO Tecnologia (Nombre, Icono) VALUES (
'NodeJs','https://i.blogs.es/c36abe/nodejs2/450_1000.png');
INSERT INTO Tecnologia (Nombre, Icono) VALUES (
'JavaScript','https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/800px-Unofficial_JavaScript_logo_2.svg.png');
INSERT INTO Tecnologia (Nombre, Icono) VALUES (
'Ruby','https://es.wikipedia.org/wiki/Ruby#/media/Archivo:Ruby_logo.svg');
INSERT INTO Tecnologia (Nombre, Icono) VALUES (
'Python','https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/200px-Python-logo-notext.svg.png');
INSERT INTO Tecnologia (Nombre, Icono) VALUES (
'Go','https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/Go_Logo_Aqua.svg/1920px-Go_Logo_Aqua.svg.png');


-- Tabla Empresa
INSERT INTO Empresa (Nombre, Correo, Celular, Direccion, Dni, FechaNacimiento, Contrasena) VALUES (
'Facebook', 'facebook@developer.com.pe', '999999990', 'Av. Silicom Voley #322', '999999999990', '2012-03-10T00:00:22.107Z', 'clave');
INSERT INTO Empresa (Nombre, Correo, Celular, Direccion, Dni, FechaNacimiento, Contrasena) VALUES (
'WhatsApp', 'whatsapp@developer.com.pe', '999999999', 'Av. EE.UU. Calle California ·123', '999999999999', '2013-06-01T00:00:22.107Z', 'clave');
INSERT INTO Empresa (Nombre, Correo, Celular, Direccion, Dni, FechaNacimiento, Contrasena) VALUES (
'Uber', 'uber@developer.com.pe', '999999998', 'Av. EE.UU. Calle Ilinois #124', '999999999998', '2015-03-01T00:00:22.107Z', 'clave');
INSERT INTO Empresa (Nombre, Correo, Celular, Direccion, Dni, FechaNacimiento, Contrasena) VALUES (
'Instagram', 'instagram@developer.com.pe', '999999997', 'Av. Peru Calle las Malvinas #169', '999999999997', '2015-09-10T00:00:22.107Z', 'clave');
INSERT INTO Empresa (Nombre, Correo, Celular, Direccion, Dni, FechaNacimiento, Contrasena) VALUES (
'Twitter', 'twitter@developer.com.pe', '999999996', 'Av. Brazil Calle las Malgonias #123', '999999999996', '2014-02-10T00:00:22.107Z', 'clave');


-- Tabla Postulante
INSERT INTO Postulante (Nombre, Apellido, Celular, Direccion, Dni, FechaNacimiento, Correo, Contrasena) VALUES (
'Jordy', 'Uzumaki Rojas', '888888880', 'Comas Av. Belaune #001', '88888880', '2000-01-01T00:00:22.107Z', 'u201512406@upce.edu.pe', 'clave');
INSERT INTO Postulante (Nombre, Apellido, Celular, Direccion, Dni, FechaNacimiento, Correo, Contrasena) VALUES (
'Fernando', 'Bedrinana Alberca', '888888889', 'Comas Av. Belaune #002', '88888889', '2000-01-01T00:00:22.107Z', 'u201614284@upce.edu.pe', 'clave');
INSERT INTO Postulante (Nombre, Apellido, Celular, Direccion, Dni, FechaNacimiento, Correo, Contrasena) VALUES (
'Pablo', 'Galarza Rosales', '888888888', 'Comas Av. Belaune #003', '88888888', '2000-01-01T00:00:22.107Z', 'u201623578@upce.edu.pe', 'clave');
INSERT INTO Postulante (Nombre, Apellido, Celular, Direccion, Dni, FechaNacimiento, Correo, Contrasena) VALUES (
'Fernando', 'Murgueytio Urbiza', '888888887', 'Comas Av. Belaune #003', '88888887', '2000-01-01T00:00:22.107Z', 'u201513031@upce.edu.pe', 'clave');


-- Tabla Nivel
INSERT INTO Nivel (Nombre) VALUES ('Basico');
INSERT INTO Nivel (Nombre) VALUES ('Medio');
INSERT INTO Nivel (Nombre) VALUES ('Intermedio');
INSERT INTO Nivel (Nombre) VALUES ('Avanzado');
INSERT INTO Nivel (Nombre) VALUES ('Experto');

-- TAbla Conocimineto
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (1,1,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (1,2,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (1,3,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (1,4,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (1,5,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (2,3,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (2,4,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (2,5,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (3,3,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (3,4,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (3,5,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (4,3,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (4,4,3);
INSERT INTO Conocimiento (PostulanteId, TecnologiaId, NivelId) VALUES (4,5,3);



-- Tabla Estado
INSERT INTO Estado (Nombre) VALUES ('Pendiente');
INSERT INTO Estado (Nombre) VALUES ('Aceptado');
INSERT INTO Estado (Nombre) VALUES ('Rechazado');


-- Tabla TipoTrabajo
INSERT INTO TipoTrabajo (Nombre, Descripcion, Comision) VALUES ('Presencial','Ser parte de una empresa hasta terminar el trabajo',0.25);
INSERT INTO TipoTrabajo (Nombre, Descripcion, Comision) VALUES ('FrontEnd','Trabajo para plataformas web',0.18);
INSERT INTO TipoTrabajo (Nombre, Descripcion, Comision) VALUES ('BackEnd','Trabajo para servicios',0.22);
INSERT INTO TipoTrabajo (Nombre, Descripcion, Comision) VALUES ('Movil','Trabajo para dispositivos Movil',0.28);


-- Tabla Trabajo
INSERT INTO Trabajo (Titulo, Descripcion, Remuneracion, FechaPublicacion, Publicado, TipoTrabajoId, EmpresaId) VALUES (
'batleFAS','Proyecto para Gamers', 2599.99, '2019-06-06T00:00:22.107Z', 0, 1, 1);
INSERT INTO Trabajo (Titulo, Descripcion, Remuneracion, FechaPublicacion, Publicado, TipoTrabajoId, EmpresaId) VALUES (
'OfertApp','Proyecto para trabajos parte servicios', 4999.99, '2019-06-10T00:00:22.107Z', 0, 3, 2);
INSERT INTO Trabajo (Titulo, Descripcion, Remuneracion, FechaPublicacion, Publicado, TipoTrabajoId, EmpresaId) VALUES (
'OfertApp','Proyecto para trabajos en moviles', 3599.99, '2019-06-10T00:00:22.107Z', 0, 4, 2);
