namespace OfertApp.Repository.Dto
{
    public class LoginPostulante
    {
        public int Id {get; set;}
        public string Correo {get; set;}
        public string Nombre {get; set;}       
    }
}