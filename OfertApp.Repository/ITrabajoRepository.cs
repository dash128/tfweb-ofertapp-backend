using System.Collections.Generic;
using OfertApp.Entity;

namespace OfertApp.Repository
{
    public interface ITrabajoRepository: ICrudRepository<Trabajo>
    {
        IEnumerable<Trabajo> TrabajoPorEmpresa(int id);

        IEnumerable<Trabajo> TrabajoPorPostulante(int id);
        IEnumerable<Trabajo> TrabajoPublicado();
        IEnumerable<Trabajo> TrabajoPublicado2(int postulanteId);
    }
}