using System.Collections.Generic;
using System.Linq;
using OfertApp.Entity;
using OfertApp.Repository.context;
using OfertApp.Repository.Dto;

namespace OfertApp.Repository.implementation
{
    public class PostulanteRepository : IPostulanteRepository
    {
        private ApplicationDbContext context;
        public PostulanteRepository(ApplicationDbContext context){
            this.context = context;
        }

        
        public bool Actualizar(Postulante entity)
        {
            try{
                var postulanteOriginal = context.Postulante.Single(p => p.Id == entity.Id);

                postulanteOriginal.Id = entity.Id;
                postulanteOriginal.Nombre = entity.Nombre;
                postulanteOriginal.Apellido = entity.Apellido;
                postulanteOriginal.Celular = entity.Celular;
                postulanteOriginal.Direccion = entity.Direccion;
                postulanteOriginal.Dni = entity.Dni;
                postulanteOriginal.FechaNacimiento = entity.FechaNacimiento;
                postulanteOriginal.Correo = entity.Correo;
                postulanteOriginal.Contrasena = entity.Contrasena;

                context.Update(postulanteOriginal);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Postulante entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Postulante> Listar()
        {
            var resultado = new List<Postulante>();

            try{
                resultado = context.Postulante.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Postulante ListarPorId(int id)
        {
            var resultado = new Postulante();

            try{
                resultado = context.Postulante.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public object ValidarCorreo(string correo, string contrasena)
        {
            var resultadoDTO = new LoginPostulante();
            var postulante = new Postulante();
            try{
              // postulante = context.Postulante.Where(p => p.Correo == correo || p.Contrasena == contrasena );
               //postulante = context.Postulante.Single(p => p.Correo == "pablo@upc.edu.pe" && p.Contrasena == "pablito2");
                postulante = context.Postulante.Single(p => p.Correo == correo && p.Contrasena == contrasena);
                resultadoDTO.Id = postulante.Id;
                resultadoDTO.Correo = postulante.Correo;
                resultadoDTO.Nombre = postulante.Nombre;
            }catch(System.Exception){
                throw;
            }
            return resultadoDTO;
        }
    }
}