using System.Collections.Generic;
using System.Linq;
using OfertApp.Entity;
using OfertApp.Repository.context;
using OfertApp.Repository.Dto;

namespace OfertApp.Repository.implementation
{
    public class EmpresaRepository : IEmpresaRepository
    {
        private ApplicationDbContext context;
        public EmpresaRepository(ApplicationDbContext context){
            this.context = context;
        }

        public bool Actualizar(Empresa entity)
        {
            try{
                var empresaOrigen = context.Empresa.Single(e => e.Id == entity.Id);

                empresaOrigen.Id = entity.Id;
                empresaOrigen.Nombre = entity.Nombre;
                empresaOrigen.Celular = entity.Celular;
                empresaOrigen.Direccion = entity.Direccion;
                empresaOrigen.Dni = entity.Dni;
                empresaOrigen.FechaNacimiento = entity.FechaNacimiento;

                context.Update(empresaOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Empresa entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Empresa> Listar()
        {
            var resultado = new List<Empresa>();

            try{
                resultado = context.Empresa.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Empresa ListarPorId(int id)
        {
            var resultado = new Empresa();

            try{
                resultado = context.Empresa.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public object ValidarCorreo(string correo, string contrasena)
        {
            var resultadoDTO = new LoginPostulante();
            var empresa = new Empresa();
            try{
                empresa = context.Empresa.Single(p => p.Correo == correo && p.Contrasena == contrasena);
                resultadoDTO.Id = empresa.Id;
                resultadoDTO.Correo = empresa.Correo;
                resultadoDTO.Nombre = empresa.Nombre;
            }catch(System.Exception){
                throw;
            }
            return resultadoDTO;
        }
    }
}