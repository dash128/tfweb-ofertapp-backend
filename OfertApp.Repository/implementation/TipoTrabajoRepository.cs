using System.Collections.Generic;
using System.Linq;
using OfertApp.Entity;
using OfertApp.Repository.context;

namespace OfertApp.Repository.implementation
{
    public class TipoTrabajoRepository : ITipoTrabajoRepository
    {
        private ApplicationDbContext context;
        public TipoTrabajoRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(TipoTrabajo entity)
        {
            try{
                var tipoTrabajoOriginal = context.TipoTrabajo.Single(t => t.Id == entity.Id);
                
                tipoTrabajoOriginal.Id = entity.Id;
                tipoTrabajoOriginal.Nombre = entity.Nombre;
                tipoTrabajoOriginal.Descripcion = entity.Descripcion;
                tipoTrabajoOriginal.Comision = entity.Comision;

                context.Update(tipoTrabajoOriginal);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(TipoTrabajo entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<TipoTrabajo> Listar()
        {
            var resultado = new List<TipoTrabajo>();

            try{
                resultado = context.TipoTrabajo.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public TipoTrabajo ListarPorId(int id)
        {
            var resultado = new TipoTrabajo();

            try{
                resultado = context.TipoTrabajo.Single(t => t.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}