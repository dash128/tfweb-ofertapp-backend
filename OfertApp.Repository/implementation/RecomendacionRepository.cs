using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository.context;

namespace OfertApp.Repository.implementation
{
    public class RecomendacionRepository : IRecomendacionRepository
    {
        private ApplicationDbContext context;
        public RecomendacionRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Recomendacion entity)
        {
            throw new System.NotImplementedException();
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Recomendacion entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Recomendacion> Listar()
        {
            throw new System.NotImplementedException();
        }

        public Recomendacion ListarPorId(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}