using System.Collections.Generic;
using System.Linq;
using OfertApp.Entity;
using OfertApp.Repository.context;
using Microsoft.EntityFrameworkCore;

namespace OfertApp.Repository.implementation
{
    public class TrabajoRepository : ITrabajoRepository
    {
        private ApplicationDbContext context;
        public TrabajoRepository(ApplicationDbContext context){
            this.context = context;
        }

        
        public bool Actualizar(Trabajo entity)
        {
            try{
                var trabajoOriginal = context.Trabajo.Single(t => t.Id == entity.Id);

                trabajoOriginal.Id = entity.Id;
                trabajoOriginal.Titulo = entity.Titulo;
                trabajoOriginal.Descripcion = entity.Descripcion;
                trabajoOriginal.FechaPublicacion = entity.FechaPublicacion;
                trabajoOriginal.Remuneracion = entity.Remuneracion;
                trabajoOriginal.Publicado = entity.Publicado;

                trabajoOriginal.TipoTrabajoId = entity.TipoTrabajoId;
                trabajoOriginal.EmpresaId = entity.EmpresaId;

                context.Update(trabajoOriginal);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Trabajo entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Trabajo> Listar()
        {
            var resultado = new List<Trabajo>();

            try{
                resultado = context.Trabajo.Include(t => t.TipoTrabajo).Include(e => e.Empresa).ToList();
                // ------z
                resultado.Select(t => new Trabajo{
                    Id = t.Id,
                    Titulo = t.Titulo,
                    Descripcion = t.Descripcion,
                    Remuneracion = t.Remuneracion,
                    Publicado = t.Publicado
                });
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Trabajo ListarPorId(int id)
        {
            var resultado = new Trabajo();

            try{
                resultado = context.Trabajo.Single(t => t.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public IEnumerable<Trabajo> TrabajoPorEmpresa(int id)
        {
            var result = new List<Trabajo>();
            
            try{
                result = context.Trabajo.Where(t => t.EmpresaId == id).ToList();
            }catch (System.Exception){
                throw;
            }
            return result;
        }

        public IEnumerable<Trabajo> TrabajoPorPostulante(int id)
        {
            var result = new List<Trabajo>();
                
            try{
                result = context.Trabajo.Where(t => t.EmpresaId == id).ToList();
            }catch (System.Exception){
                throw;
            }
            return result;
        }

        public IEnumerable<Trabajo> TrabajoPublicado()
        {
            var result = new List<Trabajo>();
                
            try{
                result = context.Trabajo.Where(t => t.Publicado == true).ToList();
            }catch (System.Exception){
                throw;
            }
            return result;
        }

        public IEnumerable<Trabajo> TrabajoPublicado2(int postulanteId)
        {
            var result = new List<Trabajo>();
                
            try{
                result = context.Trabajo.Where(t => t.Publicado == true).ToList();
            }catch (System.Exception){
                throw;
            }
            return result;
        }
    }
}