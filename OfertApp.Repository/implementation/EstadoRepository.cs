using System.Collections.Generic;
using System.Linq;
using OfertApp.Entity;
using OfertApp.Repository.context;

namespace OfertApp.Repository.implementation
{
    public class EstadoRepository : IEstadoRepository
    {
        private ApplicationDbContext context;
        public EstadoRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Estado entity)
        {
            try{
                var estadoOriginal = context.Estado.Single(e => e.Id == entity.Id);
            
                estadoOriginal.Id = entity.Id;
                estadoOriginal.Nombre = entity.Nombre;

                context.Update(estadoOriginal);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Estado entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Estado> Listar()
        {
            var resultado = new List<Estado>();

            try{
                resultado = context.Estado.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Estado ListarPorId(int id)
        {
            var resultado = new Estado();
            
            try{
                resultado = context.Estado.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }    
            return resultado;
        }
    }
}