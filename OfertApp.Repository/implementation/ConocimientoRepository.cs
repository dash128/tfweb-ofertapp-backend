using System.Collections.Generic;
using System.Linq;
using OfertApp.Entity;
using OfertApp.Repository.context;
using Microsoft.EntityFrameworkCore;

namespace OfertApp.Repository.implementation
{
    public class ConocimientoRepository : IConocimientoRepository
    {        
        private ApplicationDbContext context;
        public ConocimientoRepository(ApplicationDbContext context){
            this.context = context;
        }

        
        public bool Actualizar(Conocimiento entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Conocimiento> ConocimientoPorPostulante(int id)
        {
            var resultado = new List<Conocimiento>();

            try{
                resultado = context.Conocimiento.Include(i => i.Postulante).Include(e => e.Tecnologia).Include(a => a.Nivel).Where(i => i.Postulante.Id == id).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Conocimiento entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Conocimiento> Listar()
        {
            throw new System.NotImplementedException();
        }

        public Conocimiento ListarPorId(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}