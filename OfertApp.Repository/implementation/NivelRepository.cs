using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OfertApp.Entity;
using OfertApp.Repository.context;

namespace OfertApp.Repository.implementation
{
    public class NivelRepository : INivelRepository
    {
        private ApplicationDbContext context;
        public NivelRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Nivel entity)
        {
            try{
                var nivelOriginal = context.Nivel.Single(
                    x => x.Id == entity.Id
                );

                nivelOriginal.Id = entity.Id;
                nivelOriginal.Nombre = entity.Nombre;
                
                context.Update(nivelOriginal);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Nivel entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Nivel> Listar()
        {
            var resultado = new List<Nivel>();

            try{
                resultado = context.Nivel.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Nivel ListarPorId(int id)
        {
            var resultado = new Nivel();

            try{
                resultado = context.Nivel.Single(x => x.Id == id);
            }catch (System.Exception){
                throw;
            }
            return resultado;
        }
    }
}