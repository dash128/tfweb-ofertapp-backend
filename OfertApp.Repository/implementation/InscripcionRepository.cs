using System.Collections.Generic;
using System.Linq;
using OfertApp.Entity;
using OfertApp.Repository.context;
using Microsoft.EntityFrameworkCore;

namespace OfertApp.Repository.implementation
{
    public class InscripcionRepository : IInscripcionRepository
    {
        private ApplicationDbContext context;
        public InscripcionRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Inscripcion entity)
        {
            try{
                var InscripcionOriginal = context.Inscripcion.Single(t => t.Id == entity.Id);

                InscripcionOriginal.Id = entity.Id;
                InscripcionOriginal.PostulanteId = entity.PostulanteId;
                InscripcionOriginal.TrabajoId = entity.TrabajoId;
                InscripcionOriginal.EstadoId = entity.EstadoId;

                context.Update(InscripcionOriginal);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Inscripcion entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Inscripcion> InscripcionPorEmpresa(int id)
        {
            var resultado = new List<Inscripcion>();

            try{
                resultado = context.Inscripcion.Include(i => i.Postulante).Include(e => e.Trabajo).Include(a => a.Estado).Where(i => i.Trabajo.EmpresaId == id).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public IEnumerable<Inscripcion> InscripcionPorEstadoAndPostulante(int estadoId, int postulanteId)
        {
            var resultado = new List<Inscripcion>();

            try{
                resultado = context.Inscripcion.Include(i => i.Postulante).Include(e => e.Trabajo).Include(a => a.Estado)
                .Where(i => i.PostulanteId == postulanteId ).Where(o =>  o.EstadoId==estadoId).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public IEnumerable<Inscripcion> InscripcionPorPostulante(int id)
        {
            var resultado = new List<Inscripcion>();

            try{
                resultado = context.Inscripcion.Include(i => i.Postulante).Include(e => e.Trabajo).Include(a => a.Estado).Where(i => i.PostulanteId == id).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public IEnumerable<Inscripcion> InscripcionPorTrabajo(int id)
        {
            var resultado = new List<Inscripcion>();

            try{
                resultado = context.Inscripcion.Include(i => i.Postulante).Include(e => e.Trabajo).Include(a => a.Estado).Where(i => i.TrabajoId == id).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public IEnumerable<Inscripcion> Listar()
        {
            var resultado = new List<Inscripcion>();

            try{
                resultado = context.Inscripcion.Include(i => i.Postulante).Include(e => e.Trabajo).Include(a => a.Estado).ToList();
                
                resultado.Select(i => new Inscripcion{
                    Id = i.Id,
                    PostulanteId = i.PostulanteId,
                    TrabajoId = i.TrabajoId,
                    EstadoId = i.EstadoId
                });
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Inscripcion ListarPorId(int id)
        {
            var resultado = new Inscripcion();

            try{
                resultado = context.Inscripcion.Include(i => i.Postulante).Include(e => e.Trabajo).Include(a => a.Estado).Single(i => i.Id == id);
                

                //resultado = context.Inscripcion.Single(i => i.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}