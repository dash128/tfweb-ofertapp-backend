using System.Collections.Generic;
using OfertApp.Entity;
using OfertApp.Repository.context;

namespace OfertApp.Repository.implementation
{
    public class TecnologiaRepository : ITecnologiaRepository
    {
        private ApplicationDbContext context;
        public TecnologiaRepository(ApplicationDbContext context){
            this.context = context;
        }

        
        public bool Actualizar(Tecnologia entity)
        {
            throw new System.NotImplementedException();
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Tecnologia entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Tecnologia> Listar()
        {
            throw new System.NotImplementedException();
        }

        public Tecnologia ListarPorId(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}