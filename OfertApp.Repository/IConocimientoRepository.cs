using System.Collections.Generic;
using OfertApp.Entity;

namespace OfertApp.Repository
{
    public interface IConocimientoRepository: ICrudRepository<Conocimiento>
    {
         IEnumerable<Conocimiento> ConocimientoPorPostulante(int id);
    }
}