using OfertApp.Entity;

namespace OfertApp.Repository
{
    public interface IRecomendacionRepository: ICrudRepository<Recomendacion>
    {
         
    }
}