using OfertApp.Entity;

namespace OfertApp.Repository
{
    public interface IEstadoRepository: ICrudRepository<Estado>
    {
         
    }
}