﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OfertApp.Repository.Migrations
{
    public partial class BdUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Apellido",
                table: "Empresa",
                newName: "Correo");

            migrationBuilder.AddColumn<string>(
                name: "Contrasena",
                table: "Postulante",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Correo",
                table: "Postulante",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contrasena",
                table: "Empresa",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Contrasena",
                table: "Postulante");

            migrationBuilder.DropColumn(
                name: "Correo",
                table: "Postulante");

            migrationBuilder.DropColumn(
                name: "Contrasena",
                table: "Empresa");

            migrationBuilder.RenameColumn(
                name: "Correo",
                table: "Empresa",
                newName: "Apellido");
        }
    }
}
