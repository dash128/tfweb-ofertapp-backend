﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OfertApp.Repository.Migrations
{
    public partial class bdUpdateV2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Publicado",
                table: "Trabajo",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Publicado",
                table: "Trabajo");
        }
    }
}
